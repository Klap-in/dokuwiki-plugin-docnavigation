base   docnavigation
author Gerrit Uitslag
email  klapinklapin@gmail.com
date   2023-11-20
name   DocNavigation plugin
desc   Add navigation for documentation
url    https://www.dokuwiki.org/plugin:docnavigation
